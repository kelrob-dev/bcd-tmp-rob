"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerController = void 0;
const Customer_1 = require("../models/Customer");
class CustomerController {
    index(req, res) {
        Customer_1.Customer.findAll({})
            .then((customers) => res.json(customers))
            .catch((err) => res.status(500).json(err));
    }
}
exports.CustomerController = CustomerController;
