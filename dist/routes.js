"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
const CustomerController_1 = require("./controllers/CustomerController");
const CategoryController_1 = require("./controllers/CategoryController");
const SubCategoryController_1 = require("./controllers/SubCategoryController");
class Routes {
    constructor() {
        this.customerController = new CustomerController_1.CustomerController();
        this.categoryController = new CategoryController_1.CategoryController();
        this.subCategoryController = new SubCategoryController_1.SubCategoryController();
    }
    routes(app) {
        // Customers
        app.route('/customers/all').get(this.customerController.index);
        // Categories
        app.route('/categories').get(this.categoryController.all);
        app.route('/categories/all').get(this.categoryController.index);
        app.route('/categories/create').post(this.categoryController.create);
        app.route('/categories/update/:id').put(this.categoryController.update);
        app.route('/categories/delete/:id').delete(this.categoryController.delete);
        // Sub Categories
        app.route('/sub-categories/all').get(this.subCategoryController.index);
        app.route('/sub-categories/create').post(this.subCategoryController.create);
        app.route('/sub-categories/update/:id').put(this.subCategoryController.update);
        app.route('/sub-categories/delete/:id').delete(this.subCategoryController.delete);
    }
}
exports.Routes = Routes;
