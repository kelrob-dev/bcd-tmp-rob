"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubCategory = void 0;
const sequelize_1 = require("sequelize");
const database_1 = require("../config/database");
const Category_1 = require("./Category");
class SubCategory extends sequelize_1.Model {
}
exports.SubCategory = SubCategory;
SubCategory.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    categoryId: {
        type: new sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Category_1.Category,
            key: 'CategoryId',
        },
    },
    name: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    alias: {
        type: sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
}, {
    tableName: 'subCategories',
    sequelize: database_1.database,
});
SubCategory.sync({ force: false }).then(() => console.log('Sub Categories table created'));
