"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = void 0;
const sequelize_1 = require("sequelize");
const database_1 = require("../config/database");
class Customer extends sequelize_1.Model {
}
exports.Customer = Customer;
Customer.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    fullName: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    phone: {
        type: sequelize_1.DataTypes.STRING(50),
        allowNull: false,
    },
    email: {
        type: new sequelize_1.DataTypes.STRING(200),
        allowNull: false,
    },
    password: {
        type: new sequelize_1.DataTypes.STRING(200),
        allowNull: false,
    },
}, {
    tableName: 'customers',
    sequelize: database_1.database,
});
Customer.sync({ force: false }).then(() => console.log('Customers table created'));
