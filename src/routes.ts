import { Request, Response } from 'express';
import { CustomerController } from './controllers/CustomerController';
import { CategoryController } from './controllers/CategoryController';
import { SubCategoryController } from './controllers/SubCategoryController';

export class Routes {
  public customerController: CustomerController = new CustomerController();
  public categoryController: CategoryController = new CategoryController();
  public subCategoryController: SubCategoryController = new SubCategoryController();

  public routes(app): void {

    // Customers
    app.route('/customers/all').get(this.customerController.index);

		// Categories
  	app.route('/categories').get(this.categoryController.all);
  	app.route('/categories/all').get(this.categoryController.index);
  	app.route('/categories/create').post(this.categoryController.create);
  	app.route('/categories/update/:id').put(this.categoryController.update);
  	app.route('/categories/delete/:id').delete(this.categoryController.delete);

  	// Sub Categories
    app.route('/sub-categories/all').get(this.subCategoryController.index);
    app.route('/sub-categories/create').post(this.subCategoryController.create);
    app.route('/sub-categories/update/:id').put(this.subCategoryController.update);
    app.route('/sub-categories/delete/:id').delete(this.subCategoryController.delete);
  }
}
