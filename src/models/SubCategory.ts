import { Sequelize, Model, DataTypes, BuildOptions } from 'sequelize';
import { database } from '../config/database';
import { Category } from './Category';

export class SubCategory extends Model {
  public id!: number;
  public categoryId!: number;
  public name!: string;
  public alias!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

SubCategory.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    categoryId: {
      type: new DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: Category,
        key: 'CategoryId',
      },
    },
    name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    alias: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
  },
  {
    tableName: 'subCategories',
    sequelize: database,
  },
);

SubCategory.sync({ force: false }).then(() => console.log('Sub Categories table created'));

export interface SubCategoryInterface {
  name: string;
  alias: string;
  categoryId: number;
}
