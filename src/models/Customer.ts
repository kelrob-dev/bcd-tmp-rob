import { Sequelize, Model, DataTypes, BuildOptions } from 'sequelize';
import { database } from '../config/database';

export class Customer extends Model {
  public id!: number;
  public fullName!: string;
  public phone!: string;
  public email!: string;
  public password!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Customer.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    fullName: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    phone: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    email: {
      type: new DataTypes.STRING(200),
      allowNull: false,
    },
    password: {
      type: new DataTypes.STRING(200),
      allowNull: false,
    },
  },
  {
    tableName: 'customers',
    sequelize: database,
  },
);

Customer.sync({ force: false }).then(() => console.log('Customers table created'));

export interface CustomerInterface {
  name: string;
  fullName: string;
  phone: string;
  email: string;
  password: string;
}
