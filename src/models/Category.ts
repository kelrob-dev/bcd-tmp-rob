import { Sequelize, Model, DataTypes, BuildOptions } from 'sequelize';
import { database } from '../config/database';
import { SubCategory } from './SubCategory';

export class Category extends Model {
  public id!: number;
  public name!: string;
  public alias!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Category.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    alias: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
  },
  {
    tableName: 'categories',
    sequelize: database,
  },
);

Category.hasMany(SubCategory, {
  sourceKey: 'id',
  foreignKey: 'categoryId',
  as: 'subCategories',
});

Category.sync({ force: false }).then(() => console.log('Categories table created'));

export interface CategoryInterface {
  name: string;
  alias: string;
}
