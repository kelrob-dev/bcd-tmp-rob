import { Request, Response } from 'express';
import { Customer } from '../models/Customer';

export class CustomerController {
  public index(req: Request, res: Response) {
    Customer.findAll<Customer>({})
			.then((customers: Customer[]) => res.json(customers))
			.catch((err: Error) => res.status(500).json(err));
  }
}
