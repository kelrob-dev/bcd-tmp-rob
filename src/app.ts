// Important Packages
import express, { Application, Request, Response, NextFunction } from 'express';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import { Routes } from './routes';

// Application server
const app: Application = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

const routePrv: Routes = new Routes();
routePrv.routes(app);

module.exports = app;
