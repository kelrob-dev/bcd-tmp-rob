import { Sequelize } from 'sequelize';

export const database = new Sequelize({ database: 'bcd_backend',
  dialect: 'mysql',
  storage: 'memory',
  username: 'root',
  password: 'root',
});
